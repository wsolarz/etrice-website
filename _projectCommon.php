<?php

/*******************************************************************************
 * Copyright (c) 2009 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *		Eclipse Foundation - Initial version
 *      Thomas Shuetz and Henrik Rentz-Reichert - changes for eTrice
 *    
 *******************************************************************************/

	# Set the theme for your project's web pages.
	# See the Committer Tools "How Do I" for list of themes
	# https://dev.eclipse.org/committers/
	# Optional: defaults to system theme 
	$theme = NULL;


	# Define your project-wide Navigation here
	# This appears on the left of the page if you define a left nav
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# these are optional
	
	# If you want to override the eclipse.org navigation, uncomment below.
	# $Nav->setLinkList(array());
	
	# Break the navigation into sections
	$Nav->addNavSeparator("eTrice", 	"/etrice");
	$Nav->addCustomNav("Download", "/etrice/downloads", "_self", 3);
	$Nav->addCustomNav("Documentation", "/etrice/documentation", "_blank", 3);
	$Nav->addCustomNav("Support", "/etrice/support", "_blank", 3);
	$Nav->addCustomNav("Getting Involved", "/etrice/developers", "_blank", 3);

	# Define keywords, author and title here, or in each PHP page specifically
	$pageKeywords	= "ROOM, Real-Time Object-Oriented Modeling, FSM, DSL, modeling, tool, textual, graphical, code generator, hierarchical finite state machine, inheritance";
	$pageAuthor		= "Henrik Rentz-Reichert";
	$pageTitle 		= "eTrice";
	

	# top navigation bar
	# To override and replace the navigation with your own, uncomment the line below.
 	$Menu->setMenuItemList(array());
 	$Menu->addMenuItem("Home", "/etrice", "_self");
 	$Menu->addMenuItem("Download", "/etrice/downloads", "_self");
 	$Menu->addMenuItem("Documentation", "/etrice/documentation", "_self");
 	$Menu->addMenuItem("Support", "/etrice/support", "_self");
 	$Menu->addMenuItem("Getting Involved", "/etrice/developers", "_self");
	
	# To define additional CSS or other pre-body headers
	//$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/etrice/etstyle.css"/>');
	
	# To enable occasional Eclipse Foundation Promotion banners on your pages (EclipseCon, etc)
	$App->Promotion = TRUE;
	
	# If you have Google Analytics code, use it here
	# $App->SetGoogleAnalyticsTrackingCode("YOUR_CODE");
	
	# Initialize custom solstice $variables.
	$variables = array();
	
	# Insert header navigation for project websites.
	# Bug 436108 - https://bugs.eclipse.org/bugs/show_bug.cgi?id=436108
	$links = array();
	$links[] = array(
			'icon' => 'fa-download', // Required
			'url' => '/etrice/downloads', // Required
			'title' => 'Download', // Required
			//'target' => '_blank', // Optional
			'text' => 'Eclipse Distribution, Update Site, Dropins' // Optional
	);
	
	$links[] = array(
			'icon' => 'fa-bolt', // fa-users // Required
			'url' => 'https://projects.eclipse.org/projects/modeling.mdt.etrice', // Required
			'title' => 'Activity', // Required
			//'target' => '_blank', // Optional
			'text' => 'Releases, Project Info, Committers' // Optional
	);
	
	$links[] = array(
			'icon' => 'fa-book', // Required
			'url' => '/etrice/documentation', // Required
			'title' => 'Documentation', // Required
			//'target' => '_blank', // Optional
			'text' => 'Tutorials, Examples, Videos, Online Reference' // Optional
	);
	
	$links[] = array(
			'icon' => 'fa-support', // Required
			'url' => '/etrice/support', // Required
			'title' => 'Support', // Required
			//'target' => '_blank', // Optional
			'text' => 'Bug Tracker, Newsgroup Professional Support' // Optional
	);
	
	$variables['header_nav'] = array(
		'links' =>  $links, // Required
		'logo' => array( // Required
				'src' => '/etrice/images/Etrice_CMYK.png', // Required
				//'src' => '/eclipse.org-common/themes/solstice/public/images/logo/eclipse-800x188.png',
				'alt' => 'eTrice Logo', // Optional
				'url' => 'https://www.eclipse.org/etrice/', // Optional
				//'target' => '_blank' // Optional
		),
	);
	
	// CFA Link - Big orange button in header
	$variables['btn_cfa'] = array(
		'hide' => FALSE, // Optional - Hide the CFA button.
		'html' => '', // Optional - Replace CFA html and insert custom HTML.
		'class' => 'btn btn-huge btn-info', // Optional - Replace class on CFA link.
		'href' => '//www.eclipse.org/downloads/', // Optional - Replace href on CFA link.
		'text' => '<i class="fa fa-download"></i> Download Eclipse' // Optional - Replace text of CFA link.
	);
	
	# Set Solstice theme variables. (Array)
	$App->setThemeVariables($variables);
?>