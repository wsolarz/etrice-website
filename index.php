<?php
/*******************************************************************************
 * Copyright (c) 2009 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors: Thomas Schuetz and Henrik Rentz-Reichert
 *    
 *******************************************************************************/

	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
	
	$App   = new App();
	$Nav  = new Nav();
	$Menu   = new Menu();
	
	require_once($App->getProjectCommon());
	
	# Insert extra html before closing </head> tag.
	//$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="style.css" media="screen" />');
	$App->AddExtraHtmlHeader('<meta name="description" content="eTrice: a Real-Time Object-Oriented Modeling (ROOM DSL) tool"/>');
	$App->AddExtraHtmlHeader('<meta name="title" content="eTrice: a Real-Time Object-Oriented Modeling (ROOM DSL) tool"/>');
	
	# Define these here, or in _projectCommon.php for site-wide values
	#$pageKeywords	= "eclipse, project";
	#$pageAuthor		= "Henrik Rentz-Reichert";
	$pageTitle 		= "eTrice: a Real-Time Object-Oriented Modeling (ROOM DSL) tool";
	
	ob_start();
	?>
	<div id="midcolumn">
		<?echo(file_get_contents('main/_index.html'));?>
	</div>
	<div id="rightcolumn">
		<?echo(file_get_contents('main/sidebar.html'));?>
	</div>
	<?php
	$html = ob_get_clean();
	
	# Generate the web page
	$App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);

?>