Git configuration is

[core]
	repositoryformatversion = 0
	filemode = false
	logallrefupdates = true
[remote "origin"]
	url = ssh://<user>@git.eclipse.org:29418/www.eclipse.org/etrice.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[remote "gerrit bypass ssh"]
	pushurl = ssh://<user>@git.eclipse.org:29418/www.eclipse.org/etrice.git
	push = HEAD:refs/heads/master
[branch "master"]
	remote = gerrit bypass ssh
	merge = refs/heads/master
	rebase = true
[gerrit]
	createchangeid = true
