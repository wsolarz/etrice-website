<?php  
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
	
	$App 	= new App();
	$Nav	= new Nav();
	$Menu 	= new Menu();
	
	require_once($App->getProjectCommon());

	$pageTitle 		= "Downloads";

	ob_start();
	?>
	<div class="no-left-nav">
		<?include('_index.html');?>
	</div>
	<?php
	$html = ob_get_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, null, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
