<?php  
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *		Eclipse Foundation - Initial version
 *		Henrik Rentz-Reichert - Changes for eTrice
 *		Juergen Haug - redirect to downloads
 *
 *******************************************************************************/

header("HTTP/1.1 301 Moved Permanently");
header("Location: downloads");
exit();
?>
